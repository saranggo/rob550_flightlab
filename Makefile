# Add your targets (i.e. the name of your output program) here
BINARIES = pixy_driver pixy_example pixy_example_threaded

BINARIES := $(addprefix bin/,$(BINARIES))

CC = gcc
CFLAGS = -g -Wall -lgsl -lgslcblas -lm -std=gnu99 `pkg-config --cflags lcm` -pthread
LDFLAGS = `pkg-config --libs lcm` -lgsl -lgslcblas -lm -lpthread


.PHONY: all clean lcmtypes bbblib

all: lcmtypes bbblib flightlab $(BINARIES)

lcmtypes:
	@$(MAKE) -C lcmtypes

bbblib:
	@$(MAKE) -C bbblib

clean:
	@$(MAKE) -C lcmtypes clean
	@$(MAKE) -C bbblib clean
	rm -f *~ *.o bin/*

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

lcmtypes/%.o: lcmtypes/%.c
	$(CC) $(CFLAGS) -c $^ -o $@

bin/pixy_driver: pixy_driver.o util.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

bin/pixy_example: pixy_example.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

flightlab: flightlab.o delta.o servo.o sensors.o pixy_thrd.o util.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o lcmtypes/flightlab_state_t.o bbblib/libbbb.a
	$(CC) -o $@ $^ $(LDFLAGS)

bin/pixy_example_threaded: pixy_example_threaded.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

# Add build commands for your targets here
