/* Example of receiving LCM messages */

#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <pthread.h>

#define EXTERN extern

#include "lcmtypes/pixy_frame_t.h"
#include "flightlab.h"
#include "pixy_thrd.h"
#include "sensors.h"

extern pthread_mutex_t pixy_mutex;
extern struct PixyState pixy_state;
int filter_mean_update_pixy(float *input, sensor_buffer_t *buffer, float *output);

void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{

    // Filter Variables
  	float pos[3] = {0.};
  	//float posBufferData[5][3] = {0.};
  	sensor_buffer_t posBuffer;
  	//posBuffer.data = posBufferData; 
    posBuffer.size = FILTER_BUFFER; posBuffer.index = 0;

    static unsigned long int counter;
    static unsigned long int counter_values[FILTER_BUFFER];
    static int id;  	

    // Gsl Variables
    static gsl_matrix *A;
    static gsl_vector *b;
    static gsl_vector *xx;
    static int flag;
    if(!flag){
      b = gsl_vector_alloc(6);
      xx = gsl_vector_alloc(6);
      A = gsl_matrix_alloc(6,6);
      flag = 1;
    }
   
    gsl_matrix_set_zero(A);	
 
//    fprintf(stdout,"no = %d\n",msg->nobjects);
    
    if(msg->nobjects < 3){
      pthread_mutex_lock(&pixy_mutex);
      pixy_state.flag = 99;
      pthread_mutex_unlock(&pixy_mutex);
    }
    else{
      int i,j = 0,k = 0;
      double mean_angle = 0;
      double x[3][2];
      double y[3][2];
      int vec[6] = {0};
      double f = 215.0;
      for(i = 0; (i < msg->nobjects) && (j < 6); i += 1) {
        pixy_t *obj = &msg->objects[i];

//	fprintf(stdout,"sig = %d x = %d y = %d\n",obj->signature,obj->x,obj->y);
//	fflush(stdout);

        // Prepare the Matrix
        gsl_matrix_set(A,j,0,obj->x);
        gsl_matrix_set(A,j,1,obj->y);
        gsl_matrix_set(A,j,2,1.0);
 
        gsl_matrix_set(A,j+1,3,obj->x);
        gsl_matrix_set(A,j+1,4,obj->y);
        gsl_matrix_set(A,j+1,5,1.0);

        switch(obj->signature){
          case(0213): // signature 213
	    if(vec[0]) continue;
            gsl_vector_set(b,j,P213_X);
            gsl_vector_set(b,j+1,P213_Y);
	          mean_angle += obj->angle/3.0;
            x[k][0] = obj->x;
            x[k][1] = P213_X;
            y[k][0] = obj->y;
            y[k][1] = P213_Y;
//	    printf("%o \t %d \t %d\t",0213,obj->x,obj->y);
            k++;  
        j +=2;
            break;
          case(0212): // signature 13
	    if(vec[1]) continue;
            gsl_vector_set(b,j,P212_X);
            gsl_vector_set(b,j+1,P212_Y);
	    x[k][0] = obj->x;
            x[k][1] = P212_X;
            y[k][0] = obj->y;
            y[k][1] = P212_Y;
//	    printf("%o \t %d \t %d\t",0212,obj->x,obj->y);
	    mean_angle += obj->angle/3.0;
	    k++;
        j +=2;
            break;
          case(0123): // signature 23
	    if(vec[2]) continue;
            gsl_vector_set(b,j,P123_X);
            gsl_vector_set(b,j+1,P123_Y);
	    mean_angle += obj->angle/3.0;
	    x[k][0] = obj->x;
            x[k][1] = P123_X;
            y[k][0] = obj->y;
            y[k][1] = P123_Y;
//	    printf("%o \t %d \t %d\t",0123,obj->x,obj->y);
	    k++;
        j +=2;
            break;
//          case(0232):
//	    if(vec[3]) continue;
//            gsl_vector_set(b,j,P232_X);
//            gsl_vector_set(b,j+1,P232_Y);
//	    x[k][0] = obj->x;
//            x[k][1] = P232_X;
//            y[k][0] = obj->y;
//            y[k][1] = P232_Y;
////	    printf("%o \t %d \t %d\t",0232,obj->x,obj->y);
//	          mean_angle += obj->angle/3.0;
//	          k++;
//        j +=2;
//            break;
          case(0131):
	    if(vec[4]) continue;
            gsl_vector_set(b,j,P131_X);
            gsl_vector_set(b,j+1,P131_Y);
	    mean_angle += obj->angle/3.0;
	    x[k][0] = obj->x;
            x[k][1] = P131_X;
            y[k][0] = obj->y;
            y[k][1] = P131_Y;
//	    printf("%o \t %d \t %d\t",0131,obj->x,obj->y);
	              k++;
        j +=2;
            break;
          case(0132):
	    if(vec[5]) continue;
            gsl_vector_set(b,j,P132_X);
            gsl_vector_set(b,j+1,P132_Y);
	    x[k][0] = obj->x;
            x[k][1] = P132_X;
            y[k][0] = obj->y;
            y[k][1] = P132_Y;
            mean_angle += obj->angle/3.0;
//	    printf("%o \t %d \t %d\t",0132,obj->x,obj->y);
	          k++;
         j +=2;
            break;
        }

      }

      counter++;
 
      if(k < 3){
      	pthread_mutex_lock(&pixy_mutex);
      	pixy_state.flag = 99;
      	pthread_mutex_unlock(&pixy_mutex);
//	printf("\n");
        return;
      }
        
//      for(i=0; i < 6; i++){
//	for(j = 0; j < 6; j++)
//	  fprintf(stdout,"%lf\t",gsl_matrix_get(A,i,j));
//	fprintf(stdout,"%lf\n",gsl_vector_get(b,i));
//     }
 
      // Solve the Linear System
      gsl_linalg_HH_solve(A,b,xx);

      // Get the origin coordinates
      double aux1, aux2;
      aux1 = gsl_vector_get(xx,0)*160 + gsl_vector_get(xx,1)*100 + gsl_vector_get(xx,2); 
      aux2 = gsl_vector_get(xx,3)*160 + gsl_vector_get(xx,4)*100 + gsl_vector_get(xx,5); 
      double ctheta = cos(mean_angle/180.0*3.14159);
      double stheta = sin(mean_angle/180.0*3.14159);
   //   fprintf(stdout,"Mean Angle: %lf cos: %lf sin: %lf\n ",mean_angle, ctheta, stheta);
   //   fflush(stdout);

      // Get the height
      double h[3];
      k = 0;
      for(i = 0; i < 2; i++)
        for(j = i+1; j < 3; j++){
          h[j-1 + i] = sqrt( ((x[i][1]-x[j][1])*(x[i][1] - x[j][1]) + (y[i][1]-y[j][1])*(y[i][1]-y[j][1]))/
                             ((x[i][0]-x[j][0])*(x[i][0] - x[j][0]) + (y[i][0]-y[j][0])*(y[i][0]-y[j][0])) )*f;

   //   fprintf(stdout,"H1: %lf H2: %lf H3: %lf\n ",h[0],h[1],h[2]);
   //   fflush(stdout);
	}

      pos[0] = (stheta*aux1 + ctheta*aux2); 
      pos[1] = (-ctheta*aux1 + stheta*aux2 - 105);
      pos[2] = ((h[0]+h[1]+h[2])/3.0);
      
      pthread_mutex_lock(&pixy_mutex);

      if( (counter - counter_values[id%FILTER_BUFFER]) < 30)
        pixy_state.flag = 1;
      else 
        pixy_state.flag = 50;
      counter_values[id%FILTER_BUFFER] = counter;

			filter_mean_update_pixy(pos, &posBuffer, pos);
      pixy_state.x = pos[0];
      pixy_state.y = pos[1];
      pixy_state.z = pos[2];
     pthread_mutex_unlock(&pixy_mutex);
 
      id++;
 
    }
} 

void * pixy_thrd(void * arg)
{
    lcm_t *lcm = lcm_create(NULL);
    
    pixy_frame_t_subscribe(lcm, "PIXY", pixy_handler, NULL);

    // Enter read loop
    while (1) {
        lcm_handle(lcm);
    }

    lcm_destroy(lcm);
}

int filter_mean_update_pixy(float *input, sensor_buffer_t *buffer, float *output) {
	float sum = 0.;
	float div = 0.;
	buffer->index = (buffer->index + 1) % buffer->size;
	for(int i = 0; i < 3; i++) {
		buffer->data[buffer->index][i] = input[i];
	}
	for(int i = 0; i < 3; i++) {
		sum = 0.;
		div = 0.;
		for(int j = buffer->index; j != (buffer->index-1)%buffer->size; j = (j+1)%buffer->size){
      float f = 1/((j-buffer->index)%buffer->size + 1);
			sum += buffer->data[j][i]*f;
			div+=f; //(j-index)%buffer->size
		}
		output[i] = sum/div;
	}
	return 0;
}
