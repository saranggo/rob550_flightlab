#ifndef DELTA_H_
#define DELTA_H_

#include <math.h>

int delta_calcInverse(float x0, float y0, float z0, float *theta1, float *theta2, float *theta3);
int delta_calcForward(float theta1, float theta2, float theta3, float *x0, float *y0, float *z0);

#endif /* DELTA_H_ */
