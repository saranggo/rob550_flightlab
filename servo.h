#ifndef SERVO_H_
#define SERVO_H_

#define MAX_ANGLE 90.
#define MIN_ANGLE -90.
#define MAX_DUTY_PERCENT 12.
#define MIN_DUTY_PERCENT 3.
#define USLEEP_SERVO 0000

#define PWM_PIN_0 PWM_SV1
#define PWM_PIN_1 PWM_SV2
#define PWM_PIN_2 PWM_SV3
#define PWM_PIN_3 PWM_SV4

#include <unistd.h>
#include "bbblib/bbb.h"

int initServos(void);
int stopServos(void);

int setAngleTop(int thetaNum, float theta);
int setAnglesTop(float theta0, float theta1, float theta2);

int setAngleBot(float theta);
int closeBot();
int openBot();
int isBotOpen();

int getAnglesTop(float *theta0, float *theta1, float*theta2);
int getAngleBot(float *theta);

#endif /* SERVO_H_ */
