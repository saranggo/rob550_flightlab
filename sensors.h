
#ifndef SENSORS_H_
#define SENSORS_H_

// Define gyro bias data
// ADJUST THESE FOR YOUR IMU (see read_gyro() code below)
#define GYROX_BIAS  130
#define GYROY_BIAS  -35
#define GYROZ_BIAS  750

#define FILTER_BUFFER 5

#include <pthread.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include "bbblib/bbb.h"

int startSensors(int filterType);
int stopSensors();
int getSensorValues(float *gyro, float *accel);
int getRollPitch(float *accel, float *theta, float *phi);

typedef struct sensor_buffer {
	float data[FILTER_BUFFER][3];
	int size;
	int index;
} sensor_buffer_t;

#endif /* SENSORS_H_ */
