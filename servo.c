#define EXTERN extern
#include "servo.h"

float getPercentFromAngle(float angle) {
	angle = angle > MAX_ANGLE ? MAX_ANGLE : angle;
	angle = angle < MIN_ANGLE ? MIN_ANGLE : angle;
	float angle_ratio = (angle - MIN_ANGLE) / (MAX_ANGLE - MIN_ANGLE);
	float percent = (MAX_DUTY_PERCENT - MIN_DUTY_PERCENT) * angle_ratio + MIN_DUTY_PERCENT;
//	printf("Angle: %.1f\t AngleR: %.1f\t Percent:%.1f\n", angle, angle_ratio, percent);
	return percent;
}

float getAngleFromPercent(float percent) {
	percent = percent > MAX_DUTY_PERCENT ? MAX_DUTY_PERCENT : percent;
	percent = percent < MIN_DUTY_PERCENT ? MIN_DUTY_PERCENT : percent;
	float percent_ratio = (percent - MIN_DUTY_PERCENT) / (MAX_DUTY_PERCENT - MIN_DUTY_PERCENT);
	float angle = (MAX_ANGLE - MIN_ANGLE) * percent_ratio + MIN_ANGLE;
	return angle;
}

int initPWM(int pwmPin) {
	if (bbb_initPWM(pwmPin)) {
		printf("Error initializing BBB PWM pin %d.\n", pwmPin);
		return -1;
	}
	bbb_setPeriodPWM(pwmPin, 20000000);  // Period is in nanoseconds
	bbb_setDutyPWM(pwmPin, getPercentFromAngle(MIN_ANGLE+45));  // Duty cycle percent (0-100)
	bbb_setRunStatePWM(pwmPin, pwm_run);
	usleep(10);
	return 0;
}

int initServos(void){
	initPWM(PWM_PIN_0);
	usleep(USLEEP_SERVO);
	initPWM(PWM_PIN_1);
	usleep(USLEEP_SERVO);
	initPWM(PWM_PIN_2);
	usleep(USLEEP_SERVO);
	initPWM(PWM_PIN_3);
	usleep(USLEEP_SERVO);
	return 0;
}

int stopServos(void){
	bbb_setRunStatePWM(PWM_PIN_0, pwm_stop);
	bbb_setRunStatePWM(PWM_PIN_1, pwm_stop);
	bbb_setRunStatePWM(PWM_PIN_2, pwm_stop);
	bbb_setRunStatePWM(PWM_PIN_3, pwm_stop);
	return 0;
}

int setAngleTop(int thetaNum, float theta){
	float percent = getPercentFromAngle(theta);
	switch(thetaNum){
	case 1:
		bbb_setDutyPWM(PWM_PIN_0, percent);
		break;
	case 2:
		bbb_setDutyPWM(PWM_PIN_1, percent);
		break;
	case 3:
		bbb_setDutyPWM(PWM_PIN_2, percent);
		break;
	default:
		printf("Error::set angle top != 123");
		break;
	}
	return 0;
}

int setAnglesTop(float theta1, float theta2, float theta3){
	setAngleTop(1, theta1); usleep(USLEEP_SERVO);
	setAngleTop(2, theta2); usleep(USLEEP_SERVO);
	setAngleTop(3, theta3); usleep(USLEEP_SERVO);
	return 0;
}

int setAngleBot(float theta) {
	float percent = getPercentFromAngle(theta);
	bbb_setDutyPWM(PWM_PIN_3, percent);
	return 0;
}

int closeBot() {
	return setAngleBot(60.);
}

int openBot() {
	return setAngleBot(0.);
}

int getAnglesTop(float *theta1, float *theta2, float*theta3){
	float dutyP;
	dutyP = (float)bbb_getDutyPWM(PWM_PIN_0);
	*theta1 = getAngleFromPercent(dutyP);
	dutyP = (float)bbb_getDutyPWM(PWM_PIN_1);
	*theta2 = getAngleFromPercent(dutyP);
	dutyP = (float)bbb_getDutyPWM(PWM_PIN_2);
	*theta3 = getAngleFromPercent(dutyP);
	return 0;
}

int getAngleBot(float *theta) {
	float dutyP;
	dutyP = (float)bbb_getDutyPWM(PWM_PIN_3);
	*theta = getAngleFromPercent(dutyP);
	return 0;
}

int isBotOpen() {
	float theta;
	getAngleBot(&theta);
	if(theta < 45)
		return 1;
	return 0;
}
