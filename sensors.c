#define EXTERN extern
#include "sensors.h"

int sensors_isInit = 0;
int sensors_isTerm = 0;
struct I2C_data i2cd_gyro, i2cd_accelmag;

// threads
pthread_t sensor_thread;
// for accessing the arrays
static pthread_mutex_t sensor_mutex;
static float gyroGlobal[3] = {0};
static float accelGlobal[3] = {0};

// Local functions
void read_gyro(struct I2C_data *i2cdata, float g[]);
void read_accel(struct I2C_data *i2cdata, float a[]);
void read_mag(struct I2C_data *i2cdata, float m[]);
void read_temp(struct I2C_data *i2cdata, float *temp);
int filter_median_update(float *input, sensor_buffer_t *buffer, float *output);
int filter_mean_update(float *input, sensor_buffer_t *buffer, float *output);
int filter_kalman_update(float *input, float *output);

void *sensor_process (void *data)
{
	int filterType = *((int*)data);
	float gyro[3] = {0.}, accel[3] = {0.};
//	float gyroBufferData[FILTER_BUFFER][3] = {0.}, accelBufferData[FILTER_BUFFER][3] = {0.};
	sensor_buffer_t gyroBuffer, accelBuffer;
	//gyroBuffer.data = gyroBufferData; 
  gyroBuffer.size = FILTER_BUFFER; gyroBuffer.index = 0;
	//accelBuffer.data = accelBufferData; 
  accelBuffer.size = FILTER_BUFFER; accelBuffer.index = 0;
	while(!sensors_isTerm) {
		read_gyro(&i2cd_gyro, gyro);
		//printf("gyro (deg/sec) = (%.3lf, %.3lf, %.3lf), ", gyro[0], gyro[1], gyro[2]);
		usleep(1000);

		read_accel(&i2cd_accelmag, accel);
		//printf("accel (g) = (%.3lf, %.3lf, %.3lf), ", accel[0], accel[1], accel[2]);
		usleep(1000);

		//filter here
		switch(filterType) {
		case 1:
			filter_median_update(gyro, &gyroBuffer, gyro);
			filter_median_update(accel, &accelBuffer, accel);
			break;
		case 2:
			filter_mean_update(gyro, &gyroBuffer, gyro);
			filter_mean_update(accel, &accelBuffer, accel);
			break;
		case 3:
			filter_kalman_update(gyro, gyro);
			filter_kalman_update(accel, accel);
			break;
		case 0:
		default:
			break;
		}

		if (pthread_mutex_lock(&sensor_mutex) == 0) {
			for(int i = 0; i < 3; i++) {
				gyroGlobal[i] = gyro[i];
				accelGlobal[i] = accel[i];
			}
			pthread_mutex_unlock(&sensor_mutex);
		}
	}
	return NULL;
}

int startSensors(int filterType) {
	byte buf[10];

	i2cd_gyro.name = I2C_1;  // I2C2 is enumerated as 1 on the BBB unless I2C1 is enabled
	i2cd_gyro.address = 0xD6; // Gyro address (right shifted by 1 to get 7-bit value)
	i2cd_gyro.flags = O_RDWR;

	i2cd_accelmag.name = I2C_1;  // Same I2C port as gyro
	i2cd_accelmag.address = 0x3A; // Accel/Mag address (right shifted by 1 to get 7-bit value)
	i2cd_accelmag.flags = O_RDWR;
	usleep(1000);

	if (bbb_initI2C(&i2cd_gyro)) { // Open I2C fd for gyro
		printf("Error initializing I2C port %d for gyro.\n", (int) (i2cd_gyro.name));
		return -1;
	}
	usleep(1000);

	if (bbb_initI2C(&i2cd_accelmag)) { // Open a second fd (same I2C) for accelmag
		printf("Error initializing I2C port %d for accel.\n", (int) (i2cd_accelmag.name));
		return -1;
	}
	usleep(1000);

	// Activate accelerometer, magnetometer, and gyro
	buf[0] = 0x20; buf[1] = 0x0F;
	bbb_writeI2C(&i2cd_gyro, buf, 2);
	usleep(1000);

	// XM control registers: 0x1F - 0x26
	buf[0] = 0x20; buf[1] = 0x67;  // CTRL_REG1_XM:  3=12.5Hz; 7=enable all accel ch.
	bbb_writeI2C(&i2cd_accelmag, buf, 2);
	usleep(1000);
	buf[0] = 0x24; buf[1] = 0xF0;  // Activate accel, temp through control register 5
	bbb_writeI2C(&i2cd_accelmag, buf, 2);
	usleep(1000);
	buf[0] = 0x26; buf[1] = 0x00;  // Send 0x00 to control register 7
	bbb_writeI2C(&i2cd_accelmag, buf, 2);
	usleep(1000);

	pthread_mutex_init(&sensor_mutex, NULL);
	pthread_create (&sensor_thread, NULL, sensor_process, &filterType);

	sensors_isTerm = 0;
	sensors_isInit = 1;
	return 0;
}

int stopSensors() {
	sensors_isTerm = 1;
	pthread_join (sensor_thread, NULL);
	bbb_deinitI2C(&i2cd_gyro);
	bbb_deinitI2C(&i2cd_accelmag);
	return 0;
}

int getSensorValues(float *gyro, float *accel) {
	if (pthread_mutex_lock(&sensor_mutex) == 0) {
		for(int i = 0; i < 3; i++) {
			gyro[i] = gyroGlobal[i];
			accel[i] = accelGlobal[i];
		}
		pthread_mutex_unlock(&sensor_mutex);
	}
	return 0;
}

int getRollPitch(float *accel, float *theta, float *phi) {
	float x2, y2, z2;
	x2 = accel[0] * accel[0];
	y2 = accel[1] * accel[1];
	z2 = accel[2] * accel[2];
	// assumption x2+y2+z2=1
	*theta = atan2f(-accel[0], sqrtf(y2+z2));
	*phi = atan2f(accel[1], sqrtf(x2+z2));
	return 0;
}

int qsort_comp(const void * elem1, const void * elem2)
{
    int f = *((float*)elem1);
    int s = *((float*)elem2);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

int filter_mean_update(float *input, sensor_buffer_t *buffer, float *output) {
	float sum = 0.;
	float div = 0.;
	buffer->index = (buffer->index + 1) % buffer->size;
	for(int i = 0; i < 3; i++) {
		buffer->data[buffer->index][i] = input[i];
	}
	for(int i = 0; i < 3; i++) {
		sum = 0.;
		div = 0.;
		for(int j = buffer->index; j != (buffer->index-1)%buffer->size; j = (j+1)%buffer->size){
			sum += buffer->data[j][i];
			div++; //(j-index)%buffer->size
		}
		output[i] = sum/div;
	}
	return 0;
}

int filter_median_update(float *input, sensor_buffer_t *buffer, float *output) {
	static float tempBuf[FILTER_BUFFER] = {0.};
	buffer->index = (buffer->index + 1) % buffer->size;
	for(int i = 0; i < 3; i++) {
		buffer->data[buffer->index][i] = input[i];
	}
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < buffer->size; j++){
			tempBuf[j] = buffer->data[j][i];
		}
		qsort(tempBuf, FILTER_BUFFER, sizeof(*tempBuf), qsort_comp);
		output[i] = tempBuf[FILTER_BUFFER/2];
	}
	return 0;
}

int filter_kalman_update(float *input, float *output) {
	//TODO:
	output = input;
	return 0;
}

//////////////////////////////////////////
void read_gyro(struct I2C_data *i2cd, float gyro[])
{
	short tempint;
	byte buf, lobyte, hibyte;  // Used to store I2C data

	buf = 0x28; // X gyro, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	usleep(1000);
	bbb_readI2C(i2cd, &lobyte, 1);
	usleep(1000);
	buf = 0x29; // X gyro, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	usleep(1000);
	bbb_readI2C(i2cd, &hibyte, 1);
	usleep(1000);
	tempint = (((short) hibyte) << 8) | lobyte;
	// GYROX_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your x-gyro
	// printf("gyro x=%hd\n", tempint+GYROX_BIAS); // With your bias this should be near zero
	gyro[0] = 0.00875*(tempint + GYROX_BIAS);  // 110 is the zero bias/offset - please adjust

	buf = 0x2A; // Y gyro, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	usleep(1000);
	bbb_readI2C(i2cd, &lobyte, 1);
	usleep(1000);
	buf = 0x2B; // Y gyro, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	usleep(1000);
	bbb_readI2C(i2cd, &hibyte, 1);
	usleep(1000);
	tempint = (((short) hibyte) << 8) | lobyte;
	// GYROY_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your y-gyro
	// printf("gyro y=%hd\n", tempint + GYROY_BIAS); // With your bias this should be near zero
	gyro[1] = 0.00875*(tempint + GYROY_BIAS);

	buf = 0x2C; // Z gyro, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	usleep(1000);
	bbb_readI2C(i2cd, &lobyte, 1);
	usleep(1000);
	buf = 0x2D; // Z gyro, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	usleep(1000);
	bbb_readI2C(i2cd, &hibyte, 1);
	usleep(1000);
	tempint = ((short) hibyte << 8) | lobyte;
	// GYROZ_BIAS is the zero bias/offset - please adjust to make tempint close to zero for your y-gyro
	// printf("gyro z=%hd\n", tempint + GYROZ_BIAS); // With your bias this should be near zero
	gyro[2] = 0.00875*(tempint + GYROZ_BIAS);

	return;
}

void read_accel(struct I2C_data *i2cd, float accel[])
{
	short tempint;
	byte buf, lobyte, hibyte;  // Used to store I2C data

	buf = 0x28; // X accel, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &lobyte, 1);
	buf = 0x29; // X accel, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &hibyte, 1);
	tempint = (((short) hibyte) << 8) | lobyte;
	accel[0] = 0.000061*tempint;    // Not sure about negative readings yet???

	buf = 0x2A; // Y accel, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &lobyte, 1);
	buf = 0x2B; // Y accel, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &hibyte, 1);
	tempint = (((short) hibyte) << 8) | lobyte;
	accel[1] = 0.000061*tempint;

	buf = 0x2C; // Z accel, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &lobyte, 1);
	buf = 0x2D; // Z accel, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &hibyte, 1);
	tempint = (((short) hibyte) << 8) | lobyte;
	accel[2] = 0.000061*tempint;

	return;
}

void read_mag(struct I2C_data *i2cd, float mag[])
{
	short tempint;
	byte buf, lobyte, hibyte;  // Used to store I2C data

	buf = 0x08; // X mag, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &lobyte, 1);
	buf = 0x09; // X mag, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &hibyte, 1);
	tempint = (((short) hibyte) << 8) | lobyte;
	mag[0] = 0.00008*tempint;

	buf = 0x0A; // Y mag, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &lobyte, 1);
	buf = 0x0B; // Y mag, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &hibyte, 1);
	tempint = (((short) hibyte) << 8) | lobyte;
	mag[1] = 0.00008*tempint;

	buf = 0x0C; // Z mag, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &lobyte, 1);
	buf = 0x0D; // Z mag, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &hibyte, 1);
	tempint = (((short) hibyte) << 8) | lobyte;
	mag[2] = 0.00008*tempint;

	return;
}

void read_temp(struct I2C_data *i2cd, float *temp)
{
	short tempint;
	byte buf, lobyte, hibyte;  // Used to store I2C data

	buf = 0x05; // Z mag, low byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &lobyte, 1);
	buf = 0x06; // Z mag, high byte request
	bbb_writeI2C(i2cd, &buf, 1);
	bbb_readI2C(i2cd, &hibyte, 1);
	tempint = (((short) hibyte) << 8) | lobyte;
	// printf("Temp = %hd\n", tempint);
	*temp = (float) tempint;

	return;
}
