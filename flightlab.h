#ifndef _FLIGHTLAB_H_
#define _FLIGHTLAB_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <stdint.h>
#include <sys/select.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <lcm/lcm.h>

#include "util.h"
#include "delta.h"
#include "bbblib/bbb.h"
#include "servo.h"
#include "sensors.h"
#include "lcmtypes/pixy_frame_t.h"
#include "lcmtypes/flightlab_state_t.h"

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
#define MAX_X 7
#define MIN_X -7
#define MAX_Y 10
#define MIN_Y -10
#define MAX_Z -90
#define MIN_Z -200
#define DIA_BALL 70

struct PixyState{
  int flag;
  int k;
  float x;
  float y;
  float z;
}; 

typedef struct {
	char input_cmd;
	pthread_t cmd_thread;
	pthread_mutex_t cmd_mutex;
	int64_t heartBeat;
	int onceDisplayBaseCoor;

	int cmdTerm;
	int cmdFetch;
	int cmdStow;
	int cmdDrop;

	int canLocalize;
	int canGoTop;
	int canGoBottom;
	int isAtTop;
	int isAtBottom;
	int isParallel;
	int hasBall;
} command_state_t;

// Global Variables
pthread_mutex_t pixy_mutex;
struct PixyState pixy_state;


void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata);
void * pixy_thrd(void * arg);

#endif // _FLIGHTLAB_H_

