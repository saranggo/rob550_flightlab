#define EXTERN
#include "flightlab.h"

void *command_process (void *data) {
	command_state_t *cmd_state = (command_state_t*)data;
	char ch;
	while(!cmd_state->cmdTerm) {
		printf("Input command: ");
		ch = getchar();
		switch(ch){
		case 'x':
			cmd_state->cmdTerm = 1;
			break;
		case 'f':
			cmd_state->cmdFetch = 1;
			cmd_state->cmdStow = 0;
			cmd_state->cmdDrop = 0;

			cmd_state->hasBall = 0;
			cmd_state->isAtBottom = 0;
			cmd_state->isAtTop = 0;
			break;
		case 's':
			cmd_state->cmdStow = 1;
			cmd_state->cmdFetch = 0;
			cmd_state->cmdDrop = 0;
			break;
		case 'd':
			cmd_state->cmdDrop = 1;
			cmd_state->cmdStow = 0;
			cmd_state->cmdFetch = 0;

			cmd_state->hasBall = 1;
			cmd_state->isAtBottom = 0;
			cmd_state->isAtTop = 0;
			break;
		case ' ':
		default:
			cmd_state->heartBeat = utime_now();
			break;
		}
		cmd_state->input_cmd = ch;
		usleep(50000);
	}
	return NULL;
}

int getPixyCoord(float *x, float *y, float *zb, float *zt) {
	pthread_mutex_lock(&pixy_mutex);
	*x = -pixy_state.x;
	*y = -pixy_state.y;
	*zb = -pixy_state.z + 170;
	*zt = *zb + DIA_BALL;
	pthread_mutex_unlock(&pixy_mutex);
	return pixy_state.flag == 1;
}

int setManipulatorPosition(float x, float y, float z, int move, float *thetas) {
	thetas[0]=-90., thetas[1]=-90., thetas[2]=-90.;
//	if(MAX_X < x || MIN_X > x || MAX_Y < y || MIN_Y > y || MAX_Z < z || MIN_Z > z) {
//		printf("x/y/z values out of bounds x:%.2f, y:%.2f, z:%.2f\n",x,y,z);
//		return -1;
//	}
	if(delta_calcInverse(x, y, z, &thetas[0], &thetas[1], &thetas[2]) == 0){
		if( (thetas[0]<-45) || (thetas[1]<-45) || (thetas[2]<-45)){
//			if (move) printf("theta too low: t1:%.2f, t2:%.2f, t3:%.2f, x:%.2f, y:%.2f, z:%.2f\n",theta1,theta2,theta3,x,y,z);
			return -1;
		}
		if( (thetas[0]>90) || (thetas[1]>90) || (thetas[2]>90)) {
//			if (move) printf("theta too low: t1:%.2f, t2:%.2f, t3:%.2f, x:%.2f, y:%.2f, z:%.2f\n",theta1,theta2,theta3,x,y,z);
			return -1;
		}
		if(move) {
			printf("t1:%.2f, t2:%.2f, t3:%.2f, x:%.2f, y:%.2f, z:%.2f\n",thetas[0],thetas[1],thetas[2],x,y,z);
			setAnglesTop(thetas[0],thetas[1],thetas[2]);
		}
		return 0;
	}
	if(move) printf("ik failed - x:%.2f y:%.2f z:%.2f\n", x, y, z);
	return -1;
}

int main (int argc, char *argv[]) {
	command_state_t cmd_state;
	cmd_state.input_cmd = '0';
	cmd_state.cmdTerm = 0;
	cmd_state.cmdDrop = 0;
	cmd_state.cmdFetch = 0;
	cmd_state.cmdStow = 0;
	pthread_mutex_init(&cmd_state.cmd_mutex, NULL);
	pthread_t pixy_thrd_id;
	int64_t timeStart = 0;
	int64_t timeExecute = 0;
	float gyro[3] = {0.}, accel[3] = {0.}, roll, pitch;
	float thetas[3];
	lcm_t *lcm = lcm_create(NULL);
	flightlab_state_t lcm_msg = {};
	const char *channel = "FLIGHTLAB_STATE";

	if (bbb_init()) {
		printf("Error initializing BBB.\n");
		return -1;
	}
	initServos();
	startSensors(1);

  	pthread_create(&pixy_thrd_id,NULL,pixy_thrd,NULL);

  	if(argc == 2) {
		if(argv[1][0] == '0') {
			printf("Setting top motors to 0 deg and quitting....\n");
			setAnglesTop(0.,0.,0.);
			sleep(3);
			return 0;
		}
		if(argv[1][0] == ':') {
			printf("Setting motors to -45/0/45/60 and quitting....\n");
			setAnglesTop(0,-10.,10.);
			setAngleBot(60.);
			sleep(3);
			return 0;
		}
		if(argv[1][0] == '-') {
			printf("Setting top motors to -45 deg and quitting....\n");
			setAnglesTop(-45.,-45.,-45.);
			sleep(3);
			return 0;
		}
		if(argv[1][0] == '+') {
			printf("Setting top motors to 90 deg and quitting....\n");
			setAnglesTop(90.,90.,90.);
			sleep(3);
			return 0;
		}
		else if(argv[1][0] == 'm') {
			float percent;
			printf("Servo motor test mode....\n");
			while(1) {
				printf("Enter duty percent (NOT angle):");
				scanf("%f",&percent);
				bbb_setDutyPWM(PWM_PIN_0, percent);
				usleep(USLEEP_SERVO);
				bbb_setDutyPWM(PWM_PIN_1, percent);
				usleep(USLEEP_SERVO);
				bbb_setDutyPWM(PWM_PIN_2, percent);
				usleep(USLEEP_SERVO);
			}
		}
		else if(argv[1][0] == 'k') {
			char cmd;
			float d_xyz=10.;
			float x,y,z,theta1,theta2,theta3;
			printf("FK/IK test mode....\n");
			setAnglesTop(-45.,-45.,-45.);
			while(1) {
				getAnglesTop(&theta1, &theta2, &theta3);
				delta_calcForward(theta1,theta2,theta3,&x,&y,&z);
				printf("Angles: %.2f %.2f %.2f, Coord: %.2f %.2f %.2f\t\t\n", theta1, theta2, theta3, x, y, z);
				scanf("%c",&cmd);
				switch(cmd){
				case 'w':
					x+=d_xyz; break;
				case 's':
					x-=d_xyz; break;
				case 'a':
					y+=d_xyz; break;
				case 'd':
					y-=d_xyz; break;
				case 'q':
					z+=d_xyz; break;
				case 'z':
					z-=d_xyz; break;
				case 'o':
					openBot(); break;
				case 'c':
					closeBot(); break;
				}
				setManipulatorPosition(x,y,z,1,thetas);
				usleep(100000);
			}
		}
		else if(argv[1][0] == 's') {
			printf("Sensor test mode....\n");
			while(1) {
				//float gyro[3] = {0.}, accel[3] = {0.};
				//float roll, pitch;
				getSensorValues(gyro, accel);
				getRollPitch(accel, &roll, &pitch);
				printf("Gyro: %.2f, %.2f, %.2f; Accel: %.2f, %.2f, %.2f; Roll: %.2f; Pitch: %.2f\t\t\r",
						gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2], roll, pitch);
				usleep(100000);
			}
		}
		else if(argv[1][0] == 'p') {
			printf("Pixy test mode....\n");
			while(1) {
				float x = 0,y = 0,zb = 0,zt = 0;
				if(getPixyCoord(&x,&y,&zb,&zt) == 1){
					printf("Test - %lf \t %lf \t %lf\n",x,y,zt);
					setManipulatorPosition(x,y,zt,1,thetas);
				}
				else
					printf("Not enough objects!\n");
				fflush(stdout);
				usleep(100000);
			}
		}
		else if(argv[1][0] == '1') {
			while(1) {
				setManipulatorPosition(0.,0.,-150.,1,thetas);
				lcm_msg.utime = utime_now();
				lcm_msg.target_x = 0;
				lcm_msg.target_y = 0;
				lcm_msg.target_z = -150;
				for(int i = 0; i < 3; i++) {
					lcm_msg.thetas[i] = thetas[i];
				}
				flightlab_state_t_publish(lcm, channel, &lcm_msg);
				sleep(1);

				setManipulatorPosition(-80.,-80.,-150.,1,thetas);
				lcm_msg.utime = utime_now();
				lcm_msg.target_x = -80;
				lcm_msg.target_y = -80;
				lcm_msg.target_z = -150;
				for(int i = 0; i < 3; i++) {
					lcm_msg.thetas[i] = thetas[i];
				}
				flightlab_state_t_publish(lcm, channel, &lcm_msg);
				sleep(1);

				setManipulatorPosition(-80.,80.,-150.,1,thetas);
				lcm_msg.utime = utime_now();
				lcm_msg.target_x = -80;
				lcm_msg.target_y = 80;
				lcm_msg.target_z = -150;
				for(int i = 0; i < 3; i++) {
					lcm_msg.thetas[i] = thetas[i];
				}
				flightlab_state_t_publish(lcm, channel, &lcm_msg);
				sleep(1);

				setManipulatorPosition(80.,80.,-150.,1,thetas);
				lcm_msg.utime = utime_now();
				lcm_msg.target_x = 80;
				lcm_msg.target_y = 80;
				lcm_msg.target_z = -150;
				for(int i = 0; i < 3; i++) {
					lcm_msg.thetas[i] = thetas[i];
				}
				flightlab_state_t_publish(lcm, channel, &lcm_msg);
				sleep(1);

				setManipulatorPosition(80.,-80.,-150.,1,thetas);
				lcm_msg.utime = utime_now();
				lcm_msg.target_x = 80;
				lcm_msg.target_y = -80;
				lcm_msg.target_z = -150;
				for(int i = 0; i < 3; i++) {
					lcm_msg.thetas[i] = thetas[i];
				}
				flightlab_state_t_publish(lcm, channel, &lcm_msg);
				sleep(1);

				setManipulatorPosition(0.,0.,-200.,1,thetas);
				lcm_msg.utime = utime_now();
				lcm_msg.target_x = 0;
				lcm_msg.target_y = 0;
				lcm_msg.target_z = -200;
				for(int i = 0; i < 3; i++) {
					lcm_msg.thetas[i] = thetas[i];
				}
				flightlab_state_t_publish(lcm, channel, &lcm_msg);
				sleep(1);
			}
		}
		else if(argv[1][0] == '2') {
			usleep(250000);
			setManipulatorPosition(0.,0.,-120.,1,thetas);
			openBot();
			usleep(250000);
			setManipulatorPosition(40.,-70.,-120.,1,thetas);
			usleep(250000);
			setManipulatorPosition(40.,-70.,-170.,1,thetas);
			usleep(250000);
			setManipulatorPosition(40.,-70.,-170.,1,thetas);
			closeBot();
			usleep(250000);
			setManipulatorPosition(40.,-70.,-120.,1,thetas);
			usleep(250000);
			setManipulatorPosition(0.,0.,-120.,1,thetas);
			sleep(3);
			setManipulatorPosition(0.,0.,-120.,1,thetas);
			openBot();
			return 0;
		}
		else if(argv[1][0] == '3') {
			float x, y, zb, zt;
			sleep(1);
			if(getPixyCoord(&x,&y,&zb,&zt) == 1){
				printf("Test - %lf \t %lf \t %lf\n",x,y,zt);
				setManipulatorPosition(x,y,zt,1,thetas);
				openBot();
				sleep(1);
				setManipulatorPosition(x,y,zb,1,thetas);
				sleep(1);
				setManipulatorPosition(x,y,zb,1,thetas);
				closeBot();
				sleep(1);
				setManipulatorPosition(x,y,zt,1,thetas);
				sleep(1);
				setManipulatorPosition(0,0,-100,1,thetas);
				sleep(3);
				setManipulatorPosition(0,0,-100,1,thetas);
				openBot();
			}
			else
				printf("Not enough objects!\n");
			return 0;
		}
	}
		
  	pthread_create(&cmd_state.cmd_thread, NULL, command_process, &cmd_state);

  	//delta_calcForward(-90.,-90.,-90.,&x,&y,&z);
	//printf("Min Z:%.1f",z);
	//delta_calcForward(90.,90.,90.,&x,&y,&z);
	//printf("Max Z:%.1f",z);
	while(!cmd_state.cmdTerm) {
		float x=0, y=0, zb=0, zt=0;
		getSensorValues(gyro, accel);
		getRollPitch(accel, &roll, &pitch);
		if(abs(roll)<0.1 && abs(pitch)<0.1)
			cmd_state.isParallel = 1;
		else
			cmd_state.isParallel = 0;
		if(cmd_state.cmdStow)
			setAnglesTop(-45.,-45.,-45.);
		else if(cmd_state.cmdFetch) {
			// put fetch logic here
			cmd_state.canLocalize = getPixyCoord(&x,&y,&zb,&zt);
			if((cmd_state.canLocalize && cmd_state.isParallel) || cmd_state.isAtBottom) {
				cmd_state.canGoTop = setManipulatorPosition(x,y,zt,0,thetas) == 0;
				cmd_state.canGoBottom = setManipulatorPosition(x,y,zb,0,thetas) == 0;
				if(((cmd_state.canGoTop && cmd_state.canGoBottom) || cmd_state.isAtBottom) && !cmd_state.hasBall){
					if(!cmd_state.isAtTop && !cmd_state.isAtBottom){
						if(timeStart == 0){
							timeStart = utime_now();
							timeExecute = 200*1000;
						}
						if(utime_now() < (timeStart + timeExecute)) {
							setManipulatorPosition(x,y,zt,1,thetas);
							openBot();
						}
						else {
							timeStart = 0;
							cmd_state.isAtTop = 1;
							cmd_state.isAtBottom = 0;
							printf("isAtTop\n");
						}
					}
					else if(cmd_state.isAtTop && !cmd_state.isAtBottom){
						if(timeStart == 0){
							timeStart = utime_now();
							timeExecute = 100*1000;
						}
						if(utime_now() < (timeStart + timeExecute)) {
							setManipulatorPosition(x,y,zb,1,thetas);
							openBot();
						}
						else {
							timeStart = 0;
							cmd_state.isAtBottom = 1;
							cmd_state.isAtTop = 0;
							printf("isAtBottom\n");
						}
					}
					else if(!cmd_state.isAtTop && cmd_state.isAtBottom){
						closeBot();
						cmd_state.hasBall = 1;
						printf("hasBall\n");
						usleep(100*1000);
					}
				}
				else if(cmd_state.hasBall) {
					cmd_state.cmdStow = 1;
					cmd_state.isAtBottom = 0;
					cmd_state.isAtTop = 0;
					printf("cmdStow\n");
				}
			}
		}
		else if(cmd_state.cmdDrop) {
			// put drop logic here
			cmd_state.canLocalize = getPixyCoord(&x,&y,&zb,&zt);
			if(cmd_state.canLocalize && cmd_state.isParallel) {
				cmd_state.canGoTop = setManipulatorPosition(x,y,(zt+zb)/2,0,thetas) == 0;
				if(cmd_state.canGoTop && cmd_state.hasBall){
					if(!cmd_state.isAtTop){
						if(timeStart == 0){
							timeStart = utime_now();
							timeExecute = 200*1000;
						}
						if(utime_now() < (timeStart + timeExecute)) {
							setManipulatorPosition(x,y,(zt+zb)/2,1,thetas);
							closeBot();
						}
						else {
							timeStart = 0;
							cmd_state.isAtTop = 1;
							printf("isAtTop\n");
						}
					}
					else {
						if(timeStart == 0){
							timeStart = utime_now();
							timeExecute = 150*1000;
						}
						if(utime_now() < (timeStart + timeExecute)) {
							setManipulatorPosition(x,y,(zt+zb)/2,1,thetas);
							openBot();
						}
						else {
							timeStart = 0;
							cmd_state.hasBall = 0;
							printf("lostBall\n");
						}
					}
				}
				else if(!cmd_state.hasBall) {
					cmd_state.cmdStow = 1;
					cmd_state.isAtBottom = 0;
					cmd_state.isAtTop = 0;
					printf("cmdStow\n");
				}
			}
		}

		// generate lcm state and publish
		lcm_msg.utime = utime_now();
		lcm_msg.target_x = x;
		lcm_msg.target_y = y;
		lcm_msg.target_z = zb;
		for(int i = 0; i < 3; i++) {
			lcm_msg.gyro[i] = gyro[i];
			lcm_msg.accel[i] = accel[i];
			lcm_msg.thetas[i] = thetas[i];
		}
		lcm_msg.target_flag = pixy_state.flag;
		lcm_msg.roll = roll;
		lcm_msg.pitch = pitch;
		lcm_msg.cmdDrop = cmd_state.cmdDrop;
		lcm_msg.cmdFetch = cmd_state.cmdFetch;
		lcm_msg.cmdStow = cmd_state.cmdStow;
		lcm_msg.cmdTerm = cmd_state.cmdTerm;
		lcm_msg.canGoBottom = cmd_state.canGoBottom;
		lcm_msg.canGoTop = cmd_state.canGoTop;
		lcm_msg.canLocalize = cmd_state.canLocalize;
		lcm_msg.isAtBottom = cmd_state.isAtBottom;
		lcm_msg.isAtTop = cmd_state.isAtTop;
		lcm_msg.isParallel = cmd_state.isParallel;
		lcm_msg.hasBall = cmd_state.hasBall;
		flightlab_state_t_publish(lcm, channel, &lcm_msg);

		usleep(10000);
	}

  	// stow manipulator
  	setAnglesTop(-45.,-45.,-45.);
  	openBot();
  	pthread_join(cmd_state.cmd_thread, NULL);
  	usleep(100000);
	return 0;
}
